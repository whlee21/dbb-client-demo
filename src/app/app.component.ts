import { Component } from '@angular/core';
// const C = require('dbb-client-js');
import { C, Greeter } from 'dbb-client-js';
// import { Greeter } from 'my-awesome-greeter';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'dbb-client-demo';
    constructor() {
        const c = new C();
        console.log(c.getX());
        console.log(Greeter('John'));
    }
}
